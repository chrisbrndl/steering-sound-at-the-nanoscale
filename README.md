# Optomechanics with COMSOL Multiphysics

This repository stores source code referenced in the appendix "Optomechanics with COMSOL Multiphysics" in the Ph.D. thesis of Christian Brendel "Steering Sound At The Nanoscale".

In particular two mph-files can be found: 
* mech_unitcell.mph: COMSOL model for the mechanical part
* opt_unitcell.mph: COMSOL model for the optical part

Moreover, several MATLAB files are included:
* calc_mech_bands.m: calculation of the mechanical band structure.
* calc_opt_bands.m : calculation of the optical band structure.
* calc_opt_TEbands.m : calculation of the optical band structure (TE-like bands only).
* calc_opt_TMbands.m: calculation of the optical band structure (TM-like bands only).
* plot_bandstructure.m: loads the saved files produced from the previous four scripts and plots the corresponding band structures (this is just a simple MATLAB code which has nothing to do with COMSOL).


