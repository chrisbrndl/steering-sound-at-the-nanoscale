%---------------------------------------------------------------
%
% 2D-snowflake optomechanical crystal
% calculation of the mechanical bandstructure
%
% the data will be saved in the file '2Dsnowflake_mech_bands.m'
%
%---------------------------------------------------------------

    clear all;
    close all;

    filename = '2Dsnowflake_mech_bands';
    
    import com.comsol.model.*
    import com.comsol.model.util.*

    model = ModelUtil.create('Model');
    model.modelNode.create('comp1');
    geometry = model.geom.create('geom1',3);

    disp('Setting Paramters... ');
    
    
    % parameters and definitions
    a = 500e-9;
    r = 200e-9;
    w = 75e-9;
    d = 220e-9;
    
    res = 3; 
    
    G = [0,0];
    M = 2*pi/a*[1/sqrt(3),0];
    K = 2*pi/a*[1/sqrt(3),1/3];
    
    kx1 = linspace(G(1),M(1),res);
    kx2 = linspace(M(1),K(1),res);
    kx3 = linspace(K(1),G(1),res); 

    ky1 = linspace(G(2),M(2),res);
    ky2 = linspace(M(2),K(2),res);
    ky3 = linspace(K(2),G(2),res); 
    
    kx = [kx1, kx2(2:end), kx3(2:end)];
    ky = [ky1, ky2(2:end), ky3(2:end)];
  
    NumberOfFrequencies = 10; 
    CentralFrequency = 0e9; 
    bands = zeros(NumberOfFrequencies,size(kx,2));
    
    
    % COMSOL-GLOBAL PARAMETERS
    model.param.set('d', num2str(d));    
    model.param.set('r', num2str(r));
    model.param.set('w', num2str(w));
    model.param.set('a', num2str(a));
    
    
    % COMSOL-GEOMETRY
    disp('Building Geometry... ');
    
    workplane = geometry.feature.create('wp1','WorkPlane');
    workplane.set('quickz','-d/2');
    
    hexagon = workplane.geom.create('pol1', 'Polygon');
    hexagon.set('source', 'table');
    hexagon.setIndex('table', 'a/sqrt(3)', 0, 0);  %value,row, coloumn
    hexagon.setIndex('table', '0', 0, 1);
    hexagon.setIndex('table', 'a/(2*sqrt(3))', 1, 0);
    hexagon.setIndex('table', 'a/2', 1, 1);
    hexagon.setIndex('table', '-a/(2*sqrt(3))', 2, 0);
    hexagon.setIndex('table', 'a/2', 2, 1);
    hexagon.setIndex('table', '-a/sqrt(3)', 3, 0);
    hexagon.setIndex('table', '0', 3, 1);
    hexagon.setIndex('table', '-a/(2*sqrt(3))', 4, 0);
    hexagon.setIndex('table', '-a/2', 4, 1);
    hexagon.setIndex('table', 'a/(2*sqrt(3))', 5, 0);
    hexagon.setIndex('table', '-a/2', 5, 1);
    
    rect1 = workplane.geom.create('r1','Rectangle');
    rect1.set('size',{'w' '2*r'});
    rect1.set('base','center');
   
    copy1 = workplane.geom.create('copy1','Copy');
    copy1.selection('input').set({'r1'});
    rot1 = workplane.geom.create('rot1', 'Rotate');
    rot1.selection('input').set({'copy1'});
    rot1.set('rot', '60');
    
    copy2 = workplane.geom.create('copy2','Copy');
    copy2.selection('input').set({'r1'});
    rot2 = workplane.geom.create('rot2', 'Rotate');
    rot2.selection('input').set({'copy2'});
    rot2.set('rot', '-60');
    
    dif = workplane.geom.create('dif1', 'Difference');
    dif.selection('input').set({'pol1'});
    dif.selection('input2').set({'r1' 'rot1' 'rot2'});
    
    ext = geometry.feature.create('ext1', 'Extrude');
    ext.set('workplane', 'wp1');
    ext.selection('input').set({'wp1'});
    ext.setIndex('distance', 'd', 0);

    geometry.run;
    %mphgeom(model);
    
    %COMSOL-MATERIAL
    disp('Setting Material...');
    Silicon = model.material.create('Silicon');
    Silicon.propertyGroup('def').set('density', '2329[kg/m^3]');
    Silicon.propertyGroup.create('Enu', 'Young''s modulus and Poisson''s ratio');
    Silicon.propertyGroup('Enu').set('poissonsratio', '0.28');
    Silicon.propertyGroup('Enu').set('youngsmodulus', '170e9[Pa]');
    Silicon.selection.set([1]);
    
    
    %COMSOL-MESH
    disp('Meshing... ');
    coord = [a/sqrt(3),0,d/2];
    temp1 = mphselectcoords(model,'geom1',coord','boundary','radius',0.01*a);
    coord = [a/(2*sqrt(3)),a/2,-d/2];
    temp2 = mphselectcoords(model,'geom1',coord','boundary','radius',0.01*a);
    l1=intersect(temp1,temp2);
    
    coord = [-a/sqrt(3),0,d/2];
    temp1 = mphselectcoords(model,'geom1',coord','boundary','radius',0.01*a);
    coord = [-a/(2*sqrt(3)),-a/2,-d/2];
    temp2 = mphselectcoords(model,'geom1',coord','boundary','radius',0.01*a);
    l2=intersect(temp1,temp2);
    
    coord = [a/(2*sqrt(3)),a/2,d/2];
    temp1 = mphselectcoords(model,'geom1',coord','boundary','radius',0.01*a);
    coord = [-a/(2*sqrt(3)),a/2,-d/2];
    temp2 = mphselectcoords(model,'geom1',coord','boundary','radius',0.01*a);
    m1=intersect(temp1,temp2);
   
    coord = [-a/(2*sqrt(3)),-a/2,d/2];
    temp1 = mphselectcoords(model,'geom1',coord','boundary','radius',0.01*a);
    coord = [a/(2*sqrt(3)),-a/2,-d/2];
    temp2 = mphselectcoords(model,'geom1',coord','boundary','radius',0.01*a);
    m2=intersect(temp1,temp2);
  
    coord = [-a/(2*sqrt(3)),a/2,d/2];
    temp1 = mphselectcoords(model,'geom1',coord','boundary','radius',0.01*a);
    coord = [-a/(sqrt(3)),0,-d/2];
    temp2 = mphselectcoords(model,'geom1',coord','boundary','radius',0.01*a);
    n1=intersect(temp1,temp2);
    
    coord = [a/(2*sqrt(3)),-a/2,d/2];
    temp1 = mphselectcoords(model,'geom1',coord','boundary','radius',0.01*a);
    coord = [a/(sqrt(3)),0,-d/2];
    temp2 = mphselectcoords(model,'geom1',coord','boundary','radius',0.01*a);
    n2=intersect(temp1,temp2);
    
    mesh = model.mesh.create('mesh1', 'geom1');
    mesh.create('ftri1', 'FreeTri');
    mesh.feature('ftri1').selection.set(l1);
    mesh.create('cpf1', 'CopyFace');
    mesh.feature('cpf1').selection('source').set(l1);
    mesh.feature('cpf1').selection('destination').set(l2);
    
    mesh.create('ftri2', 'FreeTri');
    mesh.feature('ftri2').selection.set(m1);
    mesh.create('cpf2', 'CopyFace');
    mesh.feature('cpf2').selection('source').set(m1);
    mesh.feature('cpf2').selection('destination').set(m2);
    
    mesh.create('ftri3', 'FreeTri');
    mesh.feature('ftri3').selection.set(n1);
    mesh.create('cpf3', 'CopyFace');
    mesh.feature('cpf3').selection('source').set(n1);
    mesh.feature('cpf3').selection('destination').set(n2);
    
    mesh.create('ftet1', 'FreeTet');
    mesh.feature('size').set('hauto','4');
    
    mesh.run;
    %mphmesh(model);
    
    %COMSOL PHYSICS
    disp('Setting physics... ');
    physics = model.physics.create('solid', 'SolidMechanics', 'geom1');
    
    physics.feature.create('pc1', 'PeriodicCondition', 2);
    physics.feature('pc1').set('PeriodicType', 'Floquet');
    physics.feature('pc1').selection.set([l1 l2]);
    
    physics.feature.create('pc2', 'PeriodicCondition', 2);
    physics.feature('pc2').set('PeriodicType', 'Floquet');
    physics.feature('pc2').selection.set([m1 m2]);
    
    physics.feature.create('pc3', 'PeriodicCondition', 2);
    physics.feature('pc3').set('PeriodicType', 'Floquet');
    physics.feature('pc3').selection.set([n1 n2]);
    
    
    
    %COMSOL STUDY
    disp('Launching study... ');
    study = model.study.create('std');
    studyEf = study.feature.create('eig', 'Eigenfrequency');
    studyEf.activate('solid', true);
    studyEf.set('neigs', num2str(NumberOfFrequencies));
    studyEf.set('shift', num2str(CentralFrequency));
    
   
    
    for i=1:size(kx,2)
        disp(i);
        physics.feature('pc1').set('kFloquet', {num2str(kx(i)) num2str(ky(i)) '0'});
        physics.feature('pc2').set('kFloquet', {num2str(kx(i)) num2str(ky(i)) '0'});
        physics.feature('pc3').set('kFloquet', {num2str(kx(i)) num2str(ky(i)) '0'});
        study.run;
        data = mpheval(model,'freq');
        bands(:,i)=data.d1(:,1);
    end 
    
     mphsave(model, 'test') ;
  
     %save the important variables in a binary file     
     save(filename, 'a', 'r', 'w', 'd','kx','ky','bands');
     
     
 
     

     
     
