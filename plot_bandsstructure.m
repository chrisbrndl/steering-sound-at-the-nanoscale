%---------------------------------------------------------------
%
% 2D-snowflake optomechanical crystal
% plot of the mechanical bandstructure
%
%---------------------------------------------------------------   


    clear all;
    close all;

    
    figure(1); 
    load('2Dsnowflake_mech_bands');
    
    clf
    hold on;
    for i=1:size(bands,1)
        p = plot(bands(i,:));
        p.Color = 'b';
    end
    hold off; 
    
    
    
    
    figure(2);
    load('2Dsnowflake_opt_TEbands');
    
    damp_threshold = 1e10;
    freq_threshold = 1e12;
    clf
    hold on;
    
    
    %plot lightcone
    c = 299792458;
    x = linspace(1,size(kx,2),size(kx,2));
    y = c*sqrt(kx.^2+ky.^2)/(2*pi);
    
    plot(x,y);
    
    %plot bands
    for i=1:size(freq,2)
        for j=1:size(freq(:,i),1)
                if(freq(j,i)>freq_threshold & damp(j,i)<damp_threshold & freq(j,i)<y(i))
                    
                        p = plot(i,freq(j,i));
                        p.Marker='.';
                        p.Color ='b';
                   
                end
                if(freq(j,i)<1e15 & freq(j,i)>y(i))
                    
                        p = plot(i,freq(j,i));
                        p.Marker='.';
                        p.Color ='r';
                   
                end
                
                
            
        end
    end
    
   
    hold off; 
    
    
    
    
    
    
    
    
    
    figure(3);
    load('2Dsnowflake_opt_TMbands');
    
    damp_threshold = 1e10;
    freq_threshold = 1e12;
    clf
    hold on;
    
    
    %plot lightcone
    c = 299792458;
    x = linspace(1,size(kx,2),size(kx,2));
    y = c*sqrt(kx.^2+ky.^2)/(2*pi);
    
    plot(x,y);
    
    %plot bands
    for i=1:size(freq,2)
        for j=1:size(freq(:,i),1)
                if(freq(j,i)>freq_threshold & damp(j,i)<damp_threshold & freq(j,i)<y(i))
                    
                        p = plot(i,freq(j,i));
                        p.Marker='.';
                        p.Color ='b';
                   
                end
                if(freq(j,i)<1e15 & freq(j,i)>y(i))
                    
                        p = plot(i,freq(j,i));
                        p.Marker='.';
                        p.Color ='r';
                   
                end
                
                
            
        end
    end
    
   
    hold off; 
    
    
    

   figure(4);
    load('2Dsnowflake_opt_ALLbands');
    
    damp_threshold = 1e10;
    freq_threshold = 1e12;
    clf
    hold on;
    
    
    %plot lightcone
    c = 299792458;
    x = linspace(1,size(kx,2),size(kx,2));
    y = c*sqrt(kx.^2+ky.^2)/(2*pi);
    
    plot(x,y);
    
    %plot bands
    for i=1:size(freq,2)
        for j=1:size(freq(:,i),1)
                if(freq(j,i)>freq_threshold & damp(j,i)<damp_threshold & freq(j,i)<y(i))
                    
                        p = plot(i,freq(j,i));
                        p.Marker='.';
                        p.Color ='b';
                   
                end
                if(freq(j,i)<1e15 & freq(j,i)>y(i))
                    
                        p = plot(i,freq(j,i));
                        p.Marker='.';
                        p.Color ='r';
                   
                end
                
                
            
        end
    end
    
   
    hold off; 
    
    
    
