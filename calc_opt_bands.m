%---------------------------------------------------------------
%
% 2D-snowflake optomechanical crystal
% calculation of the optical bandstructure
%
% the data will be saved in the file '2Dsnowflake_opt_bands.m'
%
%---------------------------------------------------------------

    clear all;
    close all;

    filename = '2Dsnowflake_opt_bands';
    
    import com.comsol.model.*
    import com.comsol.model.util.*

    model = ModelUtil.create('Model');
    model.modelNode.create('comp1');
    geometry = model.geom.create('geom1',3);

    disp('Setting Paramters... ');
    
    
    % parameters and definitions
    a = 500e-9;
    r = 200e-9;
    w = 75e-9;
    d = 220e-9;
    s = 3000e-9;
    
    res = 25; 
    
    G = [0,0];
    M = 2*pi/a*[1/sqrt(3),0];
    K = 2*pi/a*[1/sqrt(3),1/3];
    
    kx1 = linspace(G(1),M(1),res);
    kx2 = linspace(M(1),K(1),res);
    kx3 = linspace(K(1),G(1),res); 

    ky1 = linspace(G(2),M(2),res);
    ky2 = linspace(M(2),K(2),res);
    ky3 = linspace(K(2),G(2),res); 
    
    kx = [kx1 kx2(2:end) kx3(2:end)];
    ky = [ky1 ky2(2:end) ky3(2:end)];
  
    NumberOfFrequencies = 15; 
    CentralFrequency = 200e12;
    mat_mesh = 1; 
    air_mesh = 2; 
    
    freq = 777e20*ones(2*NumberOfFrequencies,size(kx,2));
    damp = 777e20*ones(2*NumberOfFrequencies,size(kx,2));
    
    
    % COMSOL-GLOBAL PARAMETERS
    model.param.set('d', num2str(d));    
    model.param.set('r', num2str(r));
    model.param.set('w', num2str(w));
    model.param.set('a', num2str(a));
    model.param.set('s', num2str(s));
    
    
    % COMSOL-GEOMETRY (MATERIAL)
    disp('Building Geometry... ');
    
    workplane = geometry.feature.create('wp1','WorkPlane');
    workplane.set('quickz','-d/2');
    
    hexagon = workplane.geom.create('pol1', 'Polygon');
    hexagon.set('source', 'table');
    hexagon.setIndex('table', 'a/sqrt(3)', 0, 0);  %value,row, coloumn
    hexagon.setIndex('table', '0', 0, 1);
    hexagon.setIndex('table', 'a/(2*sqrt(3))', 1, 0);
    hexagon.setIndex('table', 'a/2', 1, 1);
    hexagon.setIndex('table', '-a/(2*sqrt(3))', 2, 0);
    hexagon.setIndex('table', 'a/2', 2, 1);
    hexagon.setIndex('table', '-a/sqrt(3)', 3, 0);
    hexagon.setIndex('table', '0', 3, 1);
    hexagon.setIndex('table', '-a/(2*sqrt(3))', 4, 0);
    hexagon.setIndex('table', '-a/2', 4, 1);
    hexagon.setIndex('table', 'a/(2*sqrt(3))', 5, 0);
    hexagon.setIndex('table', '-a/2', 5, 1);
    
    rect1 = workplane.geom.create('r1','Rectangle');
    rect1.set('size',{'w' '2*r'});
    rect1.set('base','center');
   
    copy1 = workplane.geom.create('copy1','Copy');
    copy1.selection('input').set({'r1'});
    rot1 = workplane.geom.create('rot1', 'Rotate');
    rot1.selection('input').set({'copy1'});
    rot1.set('rot', '60');
    
    copy2 = workplane.geom.create('copy2','Copy');
    copy2.selection('input').set({'r1'});
    rot2 = workplane.geom.create('rot2', 'Rotate');
    rot2.selection('input').set({'copy2'});
    rot2.set('rot', '-60');
    
    dif = workplane.geom.create('dif1', 'Difference');
    dif.selection('input').set({'pol1'});
    dif.selection('input2').set({'r1' 'rot1' 'rot2'});
    
    ext = geometry.feature.create('ext1', 'Extrude');
    ext.set('workplane', 'wp1');
    ext.selection('input').set({'wp1'});
    ext.setIndex('distance', 'd', 0);
    
    
    % COMSOL-GEOMETRY (AIR)
    workplane2 = geometry.feature.create('wp2','WorkPlane');
    workplane2.set('quickz','-s');
    
    hexagon2 = workplane2.geom.create('pol2', 'Polygon');
    hexagon2.set('source', 'table');
    hexagon2.setIndex('table', 'a/sqrt(3)', 0, 0);  %value,row, coloumn
    hexagon2.setIndex('table', '0', 0, 1);
    hexagon2.setIndex('table', 'a/(2*sqrt(3))', 1, 0);
    hexagon2.setIndex('table', 'a/2', 1, 1);
    hexagon2.setIndex('table', '-a/(2*sqrt(3))', 2, 0);
    hexagon2.setIndex('table', 'a/2', 2, 1);
    hexagon2.setIndex('table', '-a/sqrt(3)', 3, 0);
    hexagon2.setIndex('table', '0', 3, 1);
    hexagon2.setIndex('table', '-a/(2*sqrt(3))', 4, 0);
    hexagon2.setIndex('table', '-a/2', 4, 1);
    hexagon2.setIndex('table', 'a/(2*sqrt(3))', 5, 0);
    hexagon2.setIndex('table', '-a/2', 5, 1);
    
    ext2 = geometry.feature.create('ext2', 'Extrude');
    ext2.set('workplane', 'wp2');
    ext2.selection('input').set({'wp2'});
    ext2.setIndex('distance', '2*s', 0);
    
    geometry.run;
    mphgeom(model);
    
    
    %COMSOL-MATERIAL
    Silicon = model.material.create('Silicon');
    Silicon.name('Silicon');
    Silicon.propertyGroup('def').set('relpermeability', '1');
    Silicon.propertyGroup('def').set('electricconductivity', '1e-12[S/m]');
    Silicon.propertyGroup('def').set('relpermittivity', '11.7');
    Silicon.selection.set([1]);
    
    Air = model.material.create('Air');
    Air.name('Air');
    Air.propertyGroup('def').set('relpermeability', '1');
    Air.propertyGroup('def').set('relpermittivity', '1');
    Air.propertyGroup('def').set('electricconductivity', '0[S/m]');
    
    coordBox = [-1,1;-1,1;-1e-6,1e-6];
    mat_domain = mphselectbox(model,'geom1',coordBox,'domain');
    Silicon.selection.set(mat_domain);
    
    coord = [0,0,s];
    air_domain = mphselectcoords(model,'geom1',coord','domain','radius',a);
    Air.selection.set(air_domain);
    
    
    %COMSOL-MESH
    disp('Meshing... ');
    mesh = model.mesh.create('mesh1', 'geom1');
    
    %material (boundary)
    coord = [a/sqrt(3),0,d/2];
    temp1 = mphselectcoords(model,'geom1',coord','boundary','radius',0.01*a);
    coord = [a/(2*sqrt(3)),a/2,-d/2];
    temp2 = mphselectcoords(model,'geom1',coord','boundary','radius',0.01*a);
    mat_1=intersect(temp1,temp2);
    
    coord = [-a/sqrt(3),0,d/2];
    temp1 = mphselectcoords(model,'geom1',coord','boundary','radius',0.01*a);
    coord = [-a/(2*sqrt(3)),-a/2,-d/2];
    temp2 = mphselectcoords(model,'geom1',coord','boundary','radius',0.01*a);
    mat_4=intersect(temp1,temp2);
    
    coord = [a/(2*sqrt(3)),a/2,d/2];
    temp1 = mphselectcoords(model,'geom1',coord','boundary','radius',0.01*a);
    coord = [-a/(2*sqrt(3)),a/2,-d/2];
    temp2 = mphselectcoords(model,'geom1',coord','boundary','radius',0.01*a);
    mat_2=intersect(temp1,temp2);
   
    coord = [-a/(2*sqrt(3)),-a/2,d/2];
    temp1 = mphselectcoords(model,'geom1',coord','boundary','radius',0.01*a);
    coord = [a/(2*sqrt(3)),-a/2,-d/2];
    temp2 = mphselectcoords(model,'geom1',coord','boundary','radius',0.01*a);
    mat_5=intersect(temp1,temp2);
  
    coord = [-a/(2*sqrt(3)),a/2,d/2];
    temp1 = mphselectcoords(model,'geom1',coord','boundary','radius',0.01*a);
    coord = [-a/(sqrt(3)),0,-d/2];
    temp2 = mphselectcoords(model,'geom1',coord','boundary','radius',0.01*a);
    mat_3=intersect(temp1,temp2);
    
    coord = [a/(2*sqrt(3)),-a/2,d/2];
    temp1 = mphselectcoords(model,'geom1',coord','boundary','radius',0.01*a);
    coord = [a/(sqrt(3)),0,-d/2];
    temp2 = mphselectcoords(model,'geom1',coord','boundary','radius',0.01*a);
    mat_6=intersect(temp1,temp2);
    
    
    mesh.create('ftri1', 'FreeTri');
    mesh.feature('ftri1').selection.set(mat_1);
    mesh.feature('ftri1').create('size1', 'Size');
    mesh.feature('ftri1').feature('size1').set('hauto', num2str(mat_mesh));
    
    mesh.create('cpf1', 'CopyFace');
    mesh.feature('cpf1').selection('source').set(mat_1);
    mesh.feature('cpf1').selection('destination').set(mat_2);

    mesh.create('cpf2', 'CopyFace');
    mesh.feature('cpf2').selection('source').set(mat_2);
    mesh.feature('cpf2').selection('destination').set(mat_3);

    mesh.create('cpf3', 'CopyFace');
    mesh.feature('cpf3').selection('source').set(mat_3);
    mesh.feature('cpf3').selection('destination').set(mat_4);

    mesh.create('cpf4', 'CopyFace');
    mesh.feature('cpf4').selection('source').set(mat_4);
    mesh.feature('cpf4').selection('destination').set(mat_5);

    mesh.create('cpf5', 'CopyFace');
    mesh.feature('cpf5').selection('source').set(mat_5);
    mesh.feature('cpf5').selection('destination').set(mat_6);

    
    %air (boundary)
        %top
    coord = [a/sqrt(3),0,s];
    temp1 = mphselectcoords(model,'geom1',coord','boundary','radius',0.01*a);
    coord = [a/(2*sqrt(3)),a/2,+d/2];
    temp2 = mphselectcoords(model,'geom1',coord','boundary','radius',0.01*a);
    air_1t=intersect(temp1,temp2);
    
    coord = [-a/sqrt(3),0,s];
    temp1 = mphselectcoords(model,'geom1',coord','boundary','radius',0.01*a);
    coord = [-a/(2*sqrt(3)),-a/2,+d/2];
    temp2 = mphselectcoords(model,'geom1',coord','boundary','radius',0.01*a);
    air_4t=intersect(temp1,temp2);
    
    coord = [a/(2*sqrt(3)),a/2,s];
    temp1 = mphselectcoords(model,'geom1',coord','boundary','radius',0.01*a);
    coord = [-a/(2*sqrt(3)),a/2,+d/2];
    temp2 = mphselectcoords(model,'geom1',coord','boundary','radius',0.01*a);
    air_2t=intersect(temp1,temp2);
   
    coord = [-a/(2*sqrt(3)),-a/2,s];
    temp1 = mphselectcoords(model,'geom1',coord','boundary','radius',0.01*a);
    coord = [a/(2*sqrt(3)),-a/2,+d/2];
    temp2 = mphselectcoords(model,'geom1',coord','boundary','radius',0.01*a);
    air_5t=intersect(temp1,temp2);
  
    coord = [-a/(2*sqrt(3)),a/2,s];
    temp1 = mphselectcoords(model,'geom1',coord','boundary','radius',0.01*a);
    coord = [-a/(sqrt(3)),0,d/2];
    temp2 = mphselectcoords(model,'geom1',coord','boundary','radius',0.01*a);
    air_3t=intersect(temp1,temp2);
    
    coord = [a/(2*sqrt(3)),-a/2,s];
    temp1 = mphselectcoords(model,'geom1',coord','boundary','radius',0.01*a);
    coord = [a/(sqrt(3)),0,+d/2];
    temp2 = mphselectcoords(model,'geom1',coord','boundary','radius',0.01*a);
    air_6t=intersect(temp1,temp2);
    
    mesh.create('ftri2', 'FreeTri');
    mesh.feature('ftri2').selection.set(air_1t);
    mesh.feature('ftri2').create('size1', 'Size');
    mesh.feature('ftri2').feature('size1').set('hauto', num2str(air_mesh));
    
    mesh.create('cpf6', 'CopyFace');
    mesh.feature('cpf6').selection('source').set(air_1t);
    mesh.feature('cpf6').selection('destination').set(air_2t);

    mesh.create('cpf7', 'CopyFace');
    mesh.feature('cpf7').selection('source').set(air_2t);
    mesh.feature('cpf7').selection('destination').set(air_3t);

    mesh.create('cpf8', 'CopyFace');
    mesh.feature('cpf8').selection('source').set(air_3t);
    mesh.feature('cpf8').selection('destination').set(air_4t);

    mesh.create('cpf9', 'CopyFace');
    mesh.feature('cpf9').selection('source').set(air_4t);
    mesh.feature('cpf9').selection('destination').set(air_5t);

    mesh.create('cpf10', 'CopyFace');
    mesh.feature('cpf10').selection('source').set(air_5t);
    mesh.feature('cpf10').selection('destination').set(air_6t);
    
    
        %bottom
    coord = [a/sqrt(3),0,-s];
    temp1 = mphselectcoords(model,'geom1',coord','boundary','radius',0.01*a);
    coord = [a/(2*sqrt(3)),a/2,-d/2];
    temp2 = mphselectcoords(model,'geom1',coord','boundary','radius',0.01*a);
    air_1b=intersect(temp1,temp2);
    
    coord = [-a/sqrt(3),0,-s];
    temp1 = mphselectcoords(model,'geom1',coord','boundary','radius',0.01*a);
    coord = [-a/(2*sqrt(3)),-a/2,-d/2];
    temp2 = mphselectcoords(model,'geom1',coord','boundary','radius',0.01*a);
    air_4b=intersect(temp1,temp2);
    
    coord = [a/(2*sqrt(3)),a/2,-s];
    temp1 = mphselectcoords(model,'geom1',coord','boundary','radius',0.01*a);
    coord = [-a/(2*sqrt(3)),a/2,-d/2];
    temp2 = mphselectcoords(model,'geom1',coord','boundary','radius',0.01*a);
    air_2b=intersect(temp1,temp2);
   
    coord = [-a/(2*sqrt(3)),-a/2,-s];
    temp1 = mphselectcoords(model,'geom1',coord','boundary','radius',0.01*a);
    coord = [a/(2*sqrt(3)),-a/2,-d/2];
    temp2 = mphselectcoords(model,'geom1',coord','boundary','radius',0.01*a);
    air_5b=intersect(temp1,temp2);
  
    coord = [-a/(2*sqrt(3)),a/2,-s];
    temp1 = mphselectcoords(model,'geom1',coord','boundary','radius',0.01*a);
    coord = [-a/(sqrt(3)),0,-d/2];
    temp2 = mphselectcoords(model,'geom1',coord','boundary','radius',0.01*a);
    air_3b=intersect(temp1,temp2);
    
    coord = [a/(2*sqrt(3)),-a/2,-s];
    temp1 = mphselectcoords(model,'geom1',coord','boundary','radius',0.01*a);
    coord = [a/(sqrt(3)),0,-d/2];
    temp2 = mphselectcoords(model,'geom1',coord','boundary','radius',0.01*a);
    air_6b=intersect(temp1,temp2);
    
    mesh.create('ftri3', 'FreeTri');
    mesh.feature('ftri3').selection.set(air_1b);
    mesh.feature('ftri3').create('size1', 'Size');
    mesh.feature('ftri3').feature('size1').set('hauto', num2str(air_mesh));
    
    mesh.create('cpf11', 'CopyFace');
    mesh.feature('cpf11').selection('source').set(air_1b);
    mesh.feature('cpf11').selection('destination').set(air_2b);

    mesh.create('cpf12', 'CopyFace');
    mesh.feature('cpf12').selection('source').set(air_2b);
    mesh.feature('cpf12').selection('destination').set(air_3b);

    mesh.create('cpf13', 'CopyFace');
    mesh.feature('cpf13').selection('source').set(air_3b);
    mesh.feature('cpf13').selection('destination').set(air_4b);

    mesh.create('cpf14', 'CopyFace');
    mesh.feature('cpf14').selection('source').set(air_4b);
    mesh.feature('cpf14').selection('destination').set(air_5b);

    mesh.create('cpf15', 'CopyFace');
    mesh.feature('cpf15').selection('source').set(air_5b);
    mesh.feature('cpf15').selection('destination').set(air_6b);
    
    
    %material(bulk)
    mesh.create('ftet1', 'FreeTet');
    mesh.feature('ftet1').selection.geom('geom1', 3);
    mesh.feature('ftet1').selection.set(mat_domain);
    mesh.feature('ftet1').create('size1', 'Size');
    mesh.feature('ftet1').feature('size1').set('hauto', num2str(mat_mesh));
    
    %air(bulk)
    mesh.create('ftet2', 'FreeTet');
    mesh.feature('ftet2').selection.geom('geom1', 3);
    mesh.feature('ftet2').selection.set(air_domain);
    mesh.feature('ftet2').create('size1', 'Size');
    mesh.feature('ftet2').feature('size1').set('hauto', num2str(air_mesh));
    
    
    mesh.run;
    mphmesh(model);
    
    
    
    %COMSOL PHYSICS
    disp('Setting Physics... ');
    
    model.physics.create('emw', 'ElectromagneticWaves', 'geom1');
    
    model.physics('emw').feature.create('pc1', 'PeriodicCondition', 2);
    model.physics('emw').feature('pc1').selection.set([air_1b, air_1t, mat_1,air_4b, air_4t, mat_4]); 
    model.physics('emw').feature('pc1').set('PeriodicType', 'Floquet');
    
    model.physics('emw').feature.create('pc2', 'PeriodicCondition', 2);
    model.physics('emw').feature('pc2').selection.set([air_2b, air_2t, mat_2,air_5b, air_5t, mat_5]); 
    model.physics('emw').feature('pc2').set('PeriodicType', 'Floquet');
    
    model.physics('emw').feature.create('pc3', 'PeriodicCondition', 2);
    model.physics('emw').feature('pc3').selection.set([air_3b, air_3t, mat_3,air_6b, air_6t, mat_6]); 
    model.physics('emw').feature('pc3').set('PeriodicType', 'Floquet');
    
    coordBox = [-1,1;-1,1;s-a,s+a];
    air_t = mphselectbox(model,'geom1',coordBox,'boundary');
    coordBox = [-1,1;-1,1;-s-a,-s+a];
    air_b = mphselectbox(model,'geom1',coordBox,'boundary');
    model.physics('emw').feature.create('sctr1', 'Scattering', 2);
    model.physics('emw').feature('sctr1').selection.set([air_t, air_b]);
    
    
    
    
    %COMSOL STUDY
    disp('Launching Study... ');
    study = model.study.create('std');
    studyEf = study.feature.create('eig', 'Eigenfrequency');
    studyEf.activate('emw', true);
    studyEf.set('neigs', num2str(NumberOfFrequencies));
    studyEf.set('shift', num2str(CentralFrequency));
    

     mphsave(model,'opt_tester');
 
    %LOOP IT
    
    
    for i=1:size(kx,2)
        disp( ['  Iteration ' ,num2str(i),'/',num2str(size(kx,2))] );
        kx_value = kx(i);
        ky_value = ky(i);
        kz_value = 0;
    
        model.physics('emw').feature('pc1').set('kFloquet', {num2str(kx_value) num2str(ky_value) num2str(kz_value)});
        model.physics('emw').feature('pc2').set('kFloquet', {num2str(kx_value) num2str(ky_value) num2str(kz_value)});
        model.physics('emw').feature('pc3').set('kFloquet', {num2str(kx_value) num2str(ky_value) num2str(kz_value)});
    
        study.run;
        data_freq = mpheval(model,'real(freq)');
        data_damp = mpheval(model,'imag(freq)');
        
        
        
        for j=1:min(size(freq,1),size(data_freq.d1,1)) 
           freq(j,i)=data_freq.d1(j,1);
           damp(j,i)=data_damp.d1(j,1); 
        end
        
        
    end
     
     
    %save the important variables in a binary file     
    save(filename, 'a', 'r', 'w', 'd','s','air_mesh','mat_mesh','kx','ky','freq', 'damp');
     
    %mphsave(model,'opt_tester');
    